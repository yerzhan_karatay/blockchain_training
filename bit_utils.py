#!/usr/bin/env python2.7
import ecdsa
import hashlib
import binascii

priv = ecdsa.SigningKey.generate(curve = ecdsa.SECP256k1)
pub = priv.get_verifying_key()

# encode base 58
def base58encode(n):
    b58 = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
    result = ''
    while n > 0:
        result = b58[n % 58] + result
        n /= 58
    return result

# decode base 58
def base256decode(s):
    result = 0
    for c in s:
        result = result * 256 + ord(c)
    return result

# counting zeroes befode base 58
def countLeadingZeroes(s):
    count = 0
    for c in s:
        if c == '\0':
            count += 1
        else:
            break
    return count

# all algorithm for base 58 encode
def base58CheckEncode(prefix, payload):
    s = chr(prefix) + payload
    checksum = hashlib.sha256(hashlib.sha256(s).digest()).digest()[0:4]
    result = s + checksum
    return '1' * countLeadingZeroes(result) + base58encode(base256decode(result))

# get private key in hex
def get_priv_str():
    return binascii.hexlify(priv.to_string()).decode('ascii').upper()

# get Wallet import format private key
def privToWif(key_hex = get_priv_str()):
    return base58CheckEncode(0x80, key_hex.decode('hex'))

# get public key in hex
def get_pub_str():
    return binascii.hexlify(pub.to_string()).decode('ascii')

# get uncompressed public key from private
def privToPub(s = get_priv_str()):
    # uncompressed public key
    sk = ecdsa.SigningKey.from_string(s.decode('hex'), curve = ecdsa.SECP256k1)
    vk = sk.verifying_key
    return ('\04' + sk.verifying_key.to_string()).encode('hex')

# get address by public key
def pubToAddr(s = privToPub(get_priv_str())):
    ripemd160 = hashlib.new('ripemd160')
    ripemd160.update(hashlib.sha256(s.decode('hex')).digest())
    return base58CheckEncode(0, ripemd160.digest())

print(privToWif())