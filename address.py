#!/usr/bin/env python2.7
import public_key as pubk
import private_key as privk
import base58check
import hashlib

def pubkToAddr(s):
    ripemd160 = hashlib.new('ripemd160')
    ripemd160.update(hashlib.sha256(s.decode('hex')).digest())
    return base58check.base58CheckEncode(0, ripemd160.digest())

print(pubkToAddr(pubk.privkToPubk(privk.get_privk_str())))