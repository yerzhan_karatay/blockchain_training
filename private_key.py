#!/usr/bin/env python2.7
import binascii
import ecdsa
import base58check

privk = ecdsa.SigningKey.generate(curve = ecdsa.SECP256k1)

def get_privk():
    return privk

def get_privk_str():
    return binascii.hexlify(privk.to_string()).decode('ascii').upper()

def privKeyToWif(key_hex):
    return base58check.base58CheckEncode(0x80, key_hex.decode('hex'))

# print(privKeyToWif(get_privk_str()).upper())

# or

# import random
# pk = ''.join(['%x' % random.randrange(16) for x in range(0,64)])

# or

# import os
# pk = os.urandom(32).encode('hex')