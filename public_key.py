#!/usr/bin/env python2.7
import binascii
import ecdsa
import private_key as privk

pubk = privk.get_privk().get_verifying_key()

def get_pubk():
    return pubk

def get_pubk_str():
    return binascii.hexlify(pubk.to_string()).decode('ascii')

def privkToPubk(s): 
    # uncompressed_public_key
    sk = ecdsa.SigningKey.from_string(s.decode('hex'), curve=ecdsa.SECP256k1)
    vk = sk.verifying_key
    return ('\04' + sk.verifying_key.to_string()).encode('hex')

print(privkToPubk(privk.get_privk_str()))
    
# import binascii
# >>> import ecdsa
# >>> private_key = ecdsa.SigningKey.generate(curve=ecdsa.SECP256k1)
# >>> public_key = private_key.get_verifying_key()
# >>> binascii.hexlify(public_key.to_string()).decode('ascii').upper()
# u'D5C08F1BFC9C26A5D18FE9254E7923DEBBD34AFB92AC23ABFC6388D2659446C1F04CCDEBB677EAABFED9294663EE79D71B57CA6A6B76BC47E6F8670FE759D746'